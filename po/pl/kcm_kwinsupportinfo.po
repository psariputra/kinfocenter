# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kinfocenter package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-19 00:46+0000\n"
"PO-Revision-Date: 2022-09-23 10:26+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.04.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Łukasz Wojniłowicz"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lukasz.wojnilowicz@gmail.com"

#: main.cpp:28
#, kde-format
msgctxt "@label kcm name"
msgid "Window Manager"
msgstr "Zarządzanie oknami"

#: main.cpp:32
#, kde-format
msgid "Nate Graham"
msgstr "Nate Graham"

#: package/contents/ui/main.qml:15
msgctxt "@info"
msgid "KWin Support Information"
msgstr "Szczegóły obsługi KWin"
