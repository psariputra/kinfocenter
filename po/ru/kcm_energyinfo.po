# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2015, 2016, 2019.
# Alexander Yavorsky <kekcuha@gmail.com>, 2019, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-19 02:06+0000\n"
"PO-Revision-Date: 2022-12-23 10:06+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.3\n"

#: package/contents/ui/main.qml:47
#, kde-format
msgid "Battery"
msgstr "Батарея"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Rechargeable"
msgstr "Перезаряжаемая"

#: package/contents/ui/main.qml:50
#, kde-format
msgid "Charge state"
msgstr "Состояние заряда"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Current charge"
msgstr "Текущий заряд"

#: package/contents/ui/main.qml:51 package/contents/ui/main.qml:52
#, kde-format
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:52
#, kde-format
msgid "Health"
msgstr "Состояние"

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Vendor"
msgstr "Поставщик"

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Model"
msgstr "Модель"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "Serial Number"
msgstr "Серийный номер"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Technology"
msgstr "Технология"

#: package/contents/ui/main.qml:60
#, kde-format
msgid "Energy"
msgstr "Заряд"

#: package/contents/ui/main.qml:62
#, kde-format
msgctxt "current power draw from the battery in W"
msgid "Consumption"
msgstr "Энергопотребление"

#: package/contents/ui/main.qml:62
#, kde-format
msgctxt "Watt"
msgid "W"
msgstr "Вт"

#: package/contents/ui/main.qml:63
#, kde-format
msgid "Voltage"
msgstr "Напряжение"

#: package/contents/ui/main.qml:63
#, kde-format
msgctxt "Volt"
msgid "V"
msgstr "В"

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Remaining energy"
msgstr "Текущий заряд"

#: package/contents/ui/main.qml:64 package/contents/ui/main.qml:65
#: package/contents/ui/main.qml:66
#, kde-format
msgctxt "Watt-hours"
msgid "Wh"
msgstr "Вт·ч"

#: package/contents/ui/main.qml:65
#, kde-format
msgid "Last full charge"
msgstr "Последний полный заряд"

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Original charge capacity"
msgstr "Исходная ёмкость батареи"

#: package/contents/ui/main.qml:70
#, kde-format
msgid "Environment"
msgstr "Среда"

#: package/contents/ui/main.qml:72
#, kde-format
msgid "Temperature"
msgstr "Температура"

#: package/contents/ui/main.qml:72
#, kde-format
msgctxt "Degree Celsius"
msgid "°C"
msgstr "°C"

#: package/contents/ui/main.qml:79
#, kde-format
msgid "Not charging"
msgstr "Не заряжается"

#: package/contents/ui/main.qml:80
#, kde-format
msgid "Charging"
msgstr "Заряжается"

#: package/contents/ui/main.qml:81
#, kde-format
msgid "Discharging"
msgstr "Разряжается"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Fully charged"
msgstr "Полностью заряжена"

#: package/contents/ui/main.qml:88
#, kde-format
msgid "Lithium ion"
msgstr "Литий-ионный"

#: package/contents/ui/main.qml:89
#, kde-format
msgid "Lithium polymer"
msgstr "Литий-полимерный"

#: package/contents/ui/main.qml:90
#, kde-format
msgid "Lithium iron phosphate"
msgstr "Литий-железо-фосфатный"

#: package/contents/ui/main.qml:91
#, kde-format
msgid "Lead acid"
msgstr "Свинцово-кислотный"

#: package/contents/ui/main.qml:92
#, kde-format
msgid "Nickel cadmium"
msgstr "Никель-кадмиевый"

#: package/contents/ui/main.qml:93
#, kde-format
msgid "Nickel metal hydride"
msgstr "Никель-металл-гидридный"

#: package/contents/ui/main.qml:94
#, kde-format
msgid "Unknown technology"
msgstr "Неизвестно"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last hour"
msgstr "Последний час"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last 2 hours"
msgstr "Последние 2 часа"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last 12 hours"
msgstr "Последние 12 часов"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last 24 hours"
msgstr "Последние 24 часа"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last 48 hours"
msgstr "Последние 48 часов"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last 7 days"
msgstr "Последние 7 дней"

#: package/contents/ui/main.qml:108
#, kde-format
msgctxt "@info:status"
msgid "No energy information available on this system"
msgstr "Информация об энергопотреблении недоступна"

#: package/contents/ui/main.qml:173
#, kde-format
msgid "Internal battery"
msgstr "Встроенная батарея"

#: package/contents/ui/main.qml:174
#, kde-format
msgid "UPS battery"
msgstr "Батарея ИБП"

#: package/contents/ui/main.qml:175
#, kde-format
msgid "Monitor battery"
msgstr "Батарея монитора"

#: package/contents/ui/main.qml:176
#, kde-format
msgid "Mouse battery"
msgstr "Батарея мыши"

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Keyboard battery"
msgstr "Батарея клавиатуры"

#: package/contents/ui/main.qml:178
#, kde-format
msgid "PDA battery"
msgstr "Батарея КПК"

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Phone battery"
msgstr "Батарея телефона"

#: package/contents/ui/main.qml:180
#, kde-format
msgid "Unknown battery"
msgstr "Неизвестная батарея"

#: package/contents/ui/main.qml:198
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1% (Charging)"
msgstr "%1% (заряжается)"

# BUGME: spacing depends on the actual unit. "%" often should not have a space in front of it. --aspotashev
#: package/contents/ui/main.qml:199
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:248
#, kde-format
msgctxt "Shorthand for Watts"
msgid "W"
msgstr " Вт"

#: package/contents/ui/main.qml:248
#, kde-format
msgctxt "literal percent sign"
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:274
#, kde-format
msgid "Charge Percentage"
msgstr "Процент заряда"

#: package/contents/ui/main.qml:284
#, kde-format
msgid "Energy Consumption"
msgstr "Энергопотребление"

#: package/contents/ui/main.qml:299
#, kde-format
msgid "Timespan"
msgstr "Промежуток времени"

#: package/contents/ui/main.qml:300
#, kde-format
msgid "Timespan of data to display"
msgstr "Показывать данные для этого промежутка времени"

#: package/contents/ui/main.qml:306
#, kde-format
msgid "Refresh"
msgstr "Обновить"

#: package/contents/ui/main.qml:318
#, kde-format
msgid "This type of history is currently not available for this device."
msgstr "Информация за этот период недоступна."

#: package/contents/ui/main.qml:376
#, kde-format
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/main.qml:387
#, kde-format
msgid "Yes"
msgstr "Да"

#: package/contents/ui/main.qml:389
#, kde-format
msgid "No"
msgstr "Нет"

# BUGME: spacing depends on the actual unit. "%" often should not have a space in front of it. --aspotashev
#: package/contents/ui/main.qml:407
#, kde-format
msgctxt "%1 is value, %2 is unit"
msgid "%1 %2"
msgstr "%1 %2"

#~ msgid "This module lets you see energy information and statistics."
#~ msgstr ""
#~ "Этот модуль позволяет просмотреть данные и статистику энергопотребления."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Александр Поташев"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "aspotashev@gmail.com"

#~ msgid "Energy Consumption Statistics"
#~ msgstr "Статистика энергопотребления"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "Kai Uwe Broulik"

#~ msgid "Application Energy Consumption"
#~ msgstr "Потребление энергии приложениями"

#~ msgid "Path: %1"
#~ msgstr "Путь: %1"

#~ msgid "PID: %1"
#~ msgstr "Идентификатор процесса (PID): %1"

#~ msgid "Wakeups per second: %1 (%2%)"
#~ msgstr "Пробуждений в секунду: %1 (%2%)"

#~ msgid "Details: %1"
#~ msgstr "Подробности: %1"

#~ msgid "Manufacturer"
#~ msgstr "Производитель"

#~ msgid "Full design"
#~ msgstr "Расчётная ёмкость"

#~ msgid "System"
#~ msgstr "Система"

#~ msgid "Has power supply"
#~ msgstr "Имеет источник питания"

#~ msgid "Capacity"
#~ msgstr "Ёмкость"
