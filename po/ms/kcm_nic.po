# kcmnic Bahasa Melayu (Malay) (ms)
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>, 2010.
# Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kcmnic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-19 02:06+0000\n"
"PO-Revision-Date: 2011-05-15 23:26+0800\n"
"Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@gmail.com>\n"
"Language-Team: Malay <kedidiemas@yahoogroups.com>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=1;\n"

#: networkmodel.cpp:159
#, fuzzy, kde-format
#| msgid "Point to Point"
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "Titik ke Titik"

#: networkmodel.cpp:166
#, fuzzy, kde-format
#| msgctxt "@item:intext Mode of network card"
#| msgid "Broadcast"
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "Broadcast"

#: networkmodel.cpp:173
#, fuzzy, kde-format
#| msgid "Multicast"
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "Multicast"

#: networkmodel.cpp:180
#, fuzzy, kde-format
#| msgid "Loopback"
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "Loopback"

#: package/contents/ui/main.qml:29
#, fuzzy, kde-format
#| msgid "Name"
msgctxt "@label"
msgid "Name:"
msgstr "Nama"

#: package/contents/ui/main.qml:33
#, fuzzy, kde-format
#| msgid "IP Address"
msgctxt "@label"
msgid "Address:"
msgstr "Alamat IP"

#: package/contents/ui/main.qml:37
#, fuzzy, kde-format
#| msgid "Network Mask"
msgctxt "@label"
msgid "Network Mask:"
msgstr "Network Mask"

#: package/contents/ui/main.qml:41
#, fuzzy, kde-format
#| msgid "Type"
msgctxt "@label"
msgid "Type:"
msgstr "Jenis"

#: package/contents/ui/main.qml:45
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr ""

#: package/contents/ui/main.qml:50
#, fuzzy, kde-format
#| msgid "State"
msgctxt "@label"
msgid "State:"
msgstr "Keadaan"

#: package/contents/ui/main.qml:58
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "Atas"

#: package/contents/ui/main.qml:59
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "Bawah"

#: package/contents/ui/main.qml:75
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr ""

#, fuzzy
#~| msgid "Network Mask"
#~ msgid "Network Interfaces"
#~ msgstr "Network Mask"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Sharuzzaman Ahmat Raslan"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sharuzzaman@gmail.com"

#, fuzzy
#~| msgid "Network Mask"
#~ msgctxt "@title:window"
#~ msgid "Network Interfaces"
#~ msgstr "Network Mask"

#, fuzzy
#~| msgid "(c) 2001 - 2002 Alexander Neundorf"
#~ msgctxt "@info"
#~ msgid "(c) 2001 - 2002 Alexander Neundorf"
#~ msgstr "(c) 2001 - 2002 Alexander Neundorf"

#, fuzzy
#~| msgid "Alexander Neundorf"
#~ msgctxt "@info:credit"
#~ msgid "Alexander Neundorf"
#~ msgstr "Alexander Neundorf"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Broadcast"
#~ msgstr "Broadcast"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Point to Point"
#~ msgstr "Titik ke Titik"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Multicast"
#~ msgstr "Multicast"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Loopback"
#~ msgstr "Loopback"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Unknown"
#~ msgstr "Tidak diketahui"

#~ msgctxt "Unknown network mask"
#~ msgid "Unknown"
#~ msgstr "Tidak diketahui"

#~ msgctxt "Unknown HWaddr"
#~ msgid "Unknown"
#~ msgstr "Tidak diketahui"

#, fuzzy
#~| msgid "Broadcast"
#~ msgctxt "@item:intable Netork type"
#~ msgid "Broadcast"
#~ msgstr "Broadcast"

#~ msgid "HWAddr"
#~ msgstr "HWAddr"

#~ msgid "&Update"
#~ msgstr "&Kemaskini"

#~ msgid "kcminfo"
#~ msgstr "kcminfo"

#~ msgid "System Information Control Module"
#~ msgstr "Modul Kawalan Maklumat Sistem"
