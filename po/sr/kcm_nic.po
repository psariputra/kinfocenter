# Translation of kcmnic.po into Serbian.
# Tiron Andric <tiron@beotel.yu>, 2003.
# Toplica Tanaskovic <toptan@kde.org.yu>, 2003, 2004.
# Chusslove Illich <caslav.ilic@gmx.net>, 2006, 2007, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcmnic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-19 02:06+0000\n"
"PO-Revision-Date: 2010-12-05 01:21+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: networkmodel.cpp:159
#, fuzzy, kde-format
#| msgid "Point to Point"
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "тачка до тачке"

#: networkmodel.cpp:166
#, fuzzy, kde-format
#| msgid "Broadcast"
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "емитовање"

#: networkmodel.cpp:173
#, fuzzy, kde-format
#| msgid "Multicast"
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "вишепренос"

#: networkmodel.cpp:180
#, fuzzy, kde-format
#| msgid "Loopback"
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "петља"

#: package/contents/ui/main.qml:29
#, fuzzy, kde-format
#| msgid "Name"
msgctxt "@label"
msgid "Name:"
msgstr "Име"

#: package/contents/ui/main.qml:33
#, fuzzy, kde-format
#| msgid "IP Address"
msgctxt "@label"
msgid "Address:"
msgstr "ИП адреса"

#: package/contents/ui/main.qml:37
#, fuzzy, kde-format
#| msgid "Network Mask"
msgctxt "@label"
msgid "Network Mask:"
msgstr "Мрежна маска"

#: package/contents/ui/main.qml:41
#, fuzzy, kde-format
#| msgid "Type"
msgctxt "@label"
msgid "Type:"
msgstr "Тип"

#: package/contents/ui/main.qml:45
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr ""

#: package/contents/ui/main.qml:50
#, fuzzy, kde-format
#| msgid "State"
msgctxt "@label"
msgid "State:"
msgstr "Стање"

#: package/contents/ui/main.qml:58
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "дигнута"

#: package/contents/ui/main.qml:59
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "спуштена"

#: package/contents/ui/main.qml:75
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr ""

#, fuzzy
#~| msgid "Network Mask"
#~ msgid "Network Interfaces"
#~ msgstr "Мрежна маска"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Тирон Андрић,Часлав Илић"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "tiron@beotel.yu,caslav.ilic@gmx.net"

# well-spelled: ХВадр
#~ msgid "HWAddr"
#~ msgstr "ХВадр"

#~ msgid "&Update"
#~ msgstr "&Ажурирај"

#~ msgid "kcminfo"
#~ msgstr "КЦМ‑инфо"

#~ msgid "System Information Control Module"
#~ msgstr ""
#~ "Контролни модул података о систему|/|$[својства дат 'Контролном модулу "
#~ "података о систему']"

#~ msgid "(c) 2001 - 2002 Alexander Neundorf"
#~ msgstr "© 2001-2002, Александер Нојндорф"

#~ msgid "Alexander Neundorf"
#~ msgstr "Александер Нојндорф"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Broadcast"
#~ msgstr "емитовање"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Point to Point"
#~ msgstr "тачка до тачке"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Multicast"
#~ msgstr "вишепренос"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Loopback"
#~ msgstr "петља"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Unknown"
#~ msgstr "непознат"

#~ msgctxt "Unknown network mask"
#~ msgid "Unknown"
#~ msgstr "непозната"

#~ msgctxt "Unknown HWaddr"
#~ msgid "Unknown"
#~ msgstr "непозната"
