# translation of kcm_vulkan.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2021.
# Matej Mrenica <matejm98mthw@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kcm_vulkan\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-17 00:29+0000\n"
"PO-Revision-Date: 2021-09-05 19:37+0200\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.1\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Roman Paholík"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wizzardsk@gmail.com"

#: main.cpp:23
#, kde-format
msgctxt "@label kcm name"
msgid "Vulkan"
msgstr "Vulkan"

#: main.cpp:24
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info"
msgid "Vulkan Graphics API Information"
msgstr "Informácie o grafickom rozhraní API Vulkan"

#~ msgctxt "@label placeholder text to filter for something"
#~ msgid "Filter…"
#~ msgstr "Filter…"

#~ msgctxt "accessible name for filter input"
#~ msgid "Filter"
#~ msgstr "Filter"
