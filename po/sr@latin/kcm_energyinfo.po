# Translation of kcm_energyinfo.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2015, 2016.
msgid ""
msgstr ""
"Project-Id-Version: kcm_energyinfo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-19 02:06+0000\n"
"PO-Revision-Date: 2016-07-03 22:52+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: package/contents/ui/main.qml:47
#, kde-format
msgid "Battery"
msgstr "Baterija"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Rechargeable"
msgstr "Punjiva"

#: package/contents/ui/main.qml:50
#, kde-format
msgid "Charge state"
msgstr "Popunjenost"

#: package/contents/ui/main.qml:51
#, fuzzy, kde-format
#| msgid "Current"
msgid "Current charge"
msgstr "Trenutno"

#: package/contents/ui/main.qml:51 package/contents/ui/main.qml:52
#, kde-format
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:52
#, kde-format
msgid "Health"
msgstr ""

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Vendor"
msgstr "Izdavač"

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Model"
msgstr "Model"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "Serial Number"
msgstr "Serijski broj"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Technology"
msgstr ""

#: package/contents/ui/main.qml:60
#, kde-format
msgid "Energy"
msgstr "Energija"

#: package/contents/ui/main.qml:62
#, kde-format
msgctxt "current power draw from the battery in W"
msgid "Consumption"
msgstr "Potrošnja"

#: package/contents/ui/main.qml:62
#, kde-format
msgctxt "Watt"
msgid "W"
msgstr "W"

#: package/contents/ui/main.qml:63
#, kde-format
msgid "Voltage"
msgstr "Napon"

#: package/contents/ui/main.qml:63
#, kde-format
msgctxt "Volt"
msgid "V"
msgstr "V"

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Remaining energy"
msgstr ""

#: package/contents/ui/main.qml:64 package/contents/ui/main.qml:65
#: package/contents/ui/main.qml:66
#, kde-format
msgctxt "Watt-hours"
msgid "Wh"
msgstr "Wh"

#: package/contents/ui/main.qml:65
#, fuzzy, kde-format
#| msgid "Last full"
msgid "Last full charge"
msgstr "Poslednje puno"

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Original charge capacity"
msgstr ""

#: package/contents/ui/main.qml:70
#, kde-format
msgid "Environment"
msgstr "Okruženje"

# >> @item sensor description
#: package/contents/ui/main.qml:72
#, kde-format
msgid "Temperature"
msgstr "Temperatura"

#: package/contents/ui/main.qml:72
#, kde-format
msgctxt "Degree Celsius"
msgid "°C"
msgstr "°C"

#: package/contents/ui/main.qml:79
#, kde-format
msgid "Not charging"
msgstr "Ne puni se"

#: package/contents/ui/main.qml:80
#, kde-format
msgid "Charging"
msgstr "Puni se"

#: package/contents/ui/main.qml:81
#, kde-format
msgid "Discharging"
msgstr "Prazni se"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Fully charged"
msgstr "Skroz puna"

#: package/contents/ui/main.qml:88
#, kde-format
msgid "Lithium ion"
msgstr ""

#: package/contents/ui/main.qml:89
#, kde-format
msgid "Lithium polymer"
msgstr ""

#: package/contents/ui/main.qml:90
#, kde-format
msgid "Lithium iron phosphate"
msgstr ""

#: package/contents/ui/main.qml:91
#, kde-format
msgid "Lead acid"
msgstr ""

#: package/contents/ui/main.qml:92
#, kde-format
msgid "Nickel cadmium"
msgstr ""

#: package/contents/ui/main.qml:93
#, kde-format
msgid "Nickel metal hydride"
msgstr ""

#: package/contents/ui/main.qml:94
#, kde-format
msgid "Unknown technology"
msgstr ""

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last hour"
msgstr "Prethodnog sata"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last 2 hours"
msgstr "Prethodna 2 sata"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last 12 hours"
msgstr "Prethodnih 12 sati"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last 24 hours"
msgstr "Prethodnih 24 sata"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last 48 hours"
msgstr "Prethodnih 48 sati"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Last 7 days"
msgstr "Prethodnih 7 dana"

#: package/contents/ui/main.qml:108
#, kde-format
msgctxt "@info:status"
msgid "No energy information available on this system"
msgstr ""

#: package/contents/ui/main.qml:173
#, kde-format
msgid "Internal battery"
msgstr ""

#: package/contents/ui/main.qml:174
#, fuzzy, kde-format
#| msgid "Battery"
msgid "UPS battery"
msgstr "Baterija"

#: package/contents/ui/main.qml:175
#, kde-format
msgid "Monitor battery"
msgstr ""

#: package/contents/ui/main.qml:176
#, fuzzy, kde-format
#| msgid "Battery"
msgid "Mouse battery"
msgstr "Baterija"

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Keyboard battery"
msgstr ""

#: package/contents/ui/main.qml:178
#, fuzzy, kde-format
#| msgid "Battery"
msgid "PDA battery"
msgstr "Baterija"

#: package/contents/ui/main.qml:179
#, fuzzy, kde-format
#| msgid "Battery"
msgid "Phone battery"
msgstr "Baterija"

#: package/contents/ui/main.qml:180
#, kde-format
msgid "Unknown battery"
msgstr ""

#: package/contents/ui/main.qml:198
#, fuzzy, kde-format
#| msgid "Charging"
msgctxt "Battery charge percentage"
msgid "%1% (Charging)"
msgstr "Puni se"

#: package/contents/ui/main.qml:199
#, fuzzy, kde-format
#| msgctxt "%1 is value, %2 is unit"
#| msgid "%1 %2"
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1 %2"

# >> @title:column
#: package/contents/ui/main.qml:248
#, kde-format
msgctxt "Shorthand for Watts"
msgid "W"
msgstr "W"

#: package/contents/ui/main.qml:248
#, kde-format
msgctxt "literal percent sign"
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:274
#, kde-format
msgid "Charge Percentage"
msgstr "Procenat popunjenosti"

#: package/contents/ui/main.qml:284
#, kde-format
msgid "Energy Consumption"
msgstr "Potrošnja energije"

#: package/contents/ui/main.qml:299
#, kde-format
msgid "Timespan"
msgstr "Vremenski raspon"

#: package/contents/ui/main.qml:300
#, kde-format
msgid "Timespan of data to display"
msgstr "Vremenski raspon za prikaz podataka"

#: package/contents/ui/main.qml:306
#, kde-format
msgid "Refresh"
msgstr "Osveži"

#: package/contents/ui/main.qml:318
#, kde-format
msgid "This type of history is currently not available for this device."
msgstr "Ova vrsta istorijata trenutno nije dostupna za ovaj uređaj."

#: package/contents/ui/main.qml:376
#, kde-format
msgid "%1:"
msgstr "%1:"

# >> @item
#: package/contents/ui/main.qml:387
#, kde-format
msgid "Yes"
msgstr "da"

# >> @item
#: package/contents/ui/main.qml:389
#, kde-format
msgid "No"
msgstr "ne"

#: package/contents/ui/main.qml:407
#, kde-format
msgctxt "%1 is value, %2 is unit"
msgid "%1 %2"
msgstr "%1 %2"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Časlav Ilić"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "caslav.ilic@gmx.net"

#~ msgid "Energy Consumption Statistics"
#~ msgstr "Statistika potrošnje energije"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "Kai Uve Broulik"

#~ msgid "Application Energy Consumption"
#~ msgstr "Potrošnja energije programa"

#~ msgid "Path: %1"
#~ msgstr "Putanja: %1"

#~ msgid "PID: %1"
#~ msgstr "PID: %1"

#~ msgid "Wakeups per second: %1 (%2%)"
#~ msgstr "Buđenja po sekundi: %1 (%2%)"

#~ msgid "Details: %1"
#~ msgstr "Detalji: %1"
