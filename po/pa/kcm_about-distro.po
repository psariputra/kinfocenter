# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-19 02:06+0000\n"
"PO-Revision-Date: 2016-08-08 14:54-0600\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: CPUEntry.cpp:17
#, kde-format
msgid "Processor:"
msgid_plural "Processors:"
msgstr[0] "ਪਰੋਸੈਸਰ:"
msgstr[1] "ਪਰੋਸੈਸਰ:"

#: CPUEntry.cpp:22
#, fuzzy, kde-format
#| msgctxt "Unknown amount of RAM"
#| msgid "Unknown"
msgctxt ""
"unknown CPU type/product name; presented as `Processors: 4 × Unknown Type'"
msgid "Unknown Type"
msgstr "ਅਣਜਾਣ"

#: GPUEntry.cpp:19
#, fuzzy, kde-format
#| msgid "Processor:"
#| msgid_plural "Processors:"
msgid "Graphics Processor:"
msgstr "ਪਰੋਸੈਸਰ:"

#: GraphicsPlatformEntry.cpp:10
#, fuzzy, kde-format
#| msgid "Processor:"
#| msgid_plural "Processors:"
msgid "Graphics Platform:"
msgstr "ਪਰੋਸੈਸਰ:"

#: KernelEntry.cpp:11
#, kde-format
msgid "Kernel Version:"
msgstr "ਕਰਨਲ ਵਰਜ਼ਨ:"

#: KernelEntry.cpp:23
#, fuzzy, kde-format
#| msgctxt "@label %1 is the CPU bit width (e.g. 32 or 64)"
#| msgid "%1-bit"
msgctxt "@label %1 is the kernel version, %2 CPU bit width (e.g. 32 or 64)"
msgid "%1 (%2-bit)"
msgstr "%1-ਬਿੱਟ"

#: main.cpp:174
#, kde-format
msgctxt "@label"
msgid "Manufacturer:"
msgstr ""

#: main.cpp:177 main.cpp:194
#, kde-format
msgctxt "@label"
msgid "Product Name:"
msgstr ""

#: main.cpp:180
#, fuzzy, kde-format
#| msgid "Qt Version:"
msgctxt "@label"
msgid "System Version:"
msgstr "Qt ਵਰਜ਼ਨ:"

#: main.cpp:183 main.cpp:197
#, kde-format
msgctxt "@label"
msgid "Serial Number:"
msgstr ""

#: main.cpp:187 main.cpp:204
#, fuzzy, kde-format
#| msgctxt "Unknown amount of RAM"
#| msgid "Unknown"
msgctxt "@label unknown entry in table"
msgid "Unknown:"
msgstr "ਅਣਜਾਣ"

#: main.cpp:200
#, fuzzy, kde-format
#| msgid "Qt Version:"
msgctxt "@label uboot is the name of a bootloader for embedded devices"
msgid "U-Boot Version:"
msgstr "Qt ਵਰਜ਼ਨ:"

#: main.cpp:238
#, kde-format
msgid "KDE Frameworks Version:"
msgstr "KDE ਫਰੇਮਵਰਕਸ ਵਰਜ਼ਨ:"

#: main.cpp:239
#, kde-format
msgid "Qt Version:"
msgstr "Qt ਵਰਜ਼ਨ:"

#: MemoryEntry.cpp:20
#, kde-format
msgid "Memory:"
msgstr "ਮੈਮੋਰੀ:"

#: MemoryEntry.cpp:49
#, kde-format
msgctxt "@label %1 is the formatted amount of system memory (e.g. 7,7 GiB)"
msgid "%1 of RAM"
msgstr "%1 ਰੈਮ"

#: MemoryEntry.cpp:53
#, kde-format
msgctxt "Unknown amount of RAM"
msgid "Unknown"
msgstr "ਅਣਜਾਣ"

#: OSVersionEntry.cpp:9
#, kde-format
msgid "Operating System:"
msgstr ""

#: OSVersionEntry.cpp:11
#, kde-format
msgctxt "@label %1 is the distro name, %2 is the version"
msgid "%1 %2"
msgstr ""

#: OSVersionEntry.cpp:13
#, kde-format
msgctxt ""
"@label %1 is the distro name, %2 is the version, %3 is the 'build' which "
"should be a number, or 'rolling'"
msgid "%1 %2 Build: %3"
msgstr ""

#: package/contents/ui/main.qml:92
#, kde-format
msgctxt "@title"
msgid "Serial Number"
msgstr ""

#: package/contents/ui/main.qml:98 package/contents/ui/main.qml:179
#, kde-format
msgctxt "@action:button"
msgid "Copy to Clipboard"
msgstr ""

#: package/contents/ui/main.qml:125
#, kde-format
msgctxt "@action:button show a hidden entry in an overlay"
msgid "Show"
msgstr ""

#: package/contents/ui/main.qml:137
#, fuzzy, kde-format
#| msgid "Software"
msgctxt "@title:group"
msgid "Software"
msgstr "ਸਾਫਟਵੇਅਰ"

#: package/contents/ui/main.qml:147
#, fuzzy, kde-format
#| msgid "Hardware"
msgctxt "@title:group"
msgid "Hardware"
msgstr "ਹਾਰਡਵੇਅਰ"

#: package/contents/ui/main.qml:167
#, kde-format
msgctxt "@action:button launches kinfocenter from systemsettings"
msgid "Launch %1"
msgstr ""

#: package/contents/ui/main.qml:186
#, kde-format
msgctxt "@action:button"
msgid "Copy to Clipboard in English"
msgstr ""

#: PlasmaEntry.cpp:15
#, kde-format
msgid "KDE Plasma Version:"
msgstr "KDE ਪਲਾਜ਼ਮਾ ਵਰਜ਼ਨ:"

#: ThirdPartyEntry.cpp:12
#, kde-format
msgctxt "Unused but needs to be : to avoid assertion in Entry constructor"
msgid ":"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ (A S Alam)"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "aalam@users.sf.net"

#, fuzzy
#~| msgctxt "@info:credit"
#~| msgid "Copyright 2012-2014 Harald Sitter"
#~ msgctxt "@info:credit"
#~ msgid "Copyright 2012-2020 Harald Sitter"
#~ msgstr "Copyright 2012-2014 Harald Sitter"

#~ msgctxt "@info:credit"
#~ msgid "Harald Sitter"
#~ msgstr "ਹਾਰਾਲਡ ਸਿੱਟਰ"

#~ msgctxt "@info:credit"
#~ msgid "Author"
#~ msgstr "ਲੇਖਕ"

#~ msgid "OS Type:"
#~ msgstr "OS ਕਿਸਮ:"

#~ msgctxt "@title"
#~ msgid "About Distribution"
#~ msgstr "ਡਿਸਟਰੀਬਿਊਸ਼ਨ ਬਾਰੇ"

#~ msgid "{variantLabel}"
#~ msgstr "{variantLabel}"
