# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <qiilaq69@gmail.com>, 2016, 2019, 2020.
# Mihkel Tõnnov <mihhkel@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-19 02:06+0000\n"
"PO-Revision-Date: 2022-11-14 01:41+0100\n"
"Last-Translator: Mihkel Tõnnov <mihhkel@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.3\n"

#: CPUEntry.cpp:17
#, kde-format
msgid "Processor:"
msgid_plural "Processors:"
msgstr[0] "Protsessor:"
msgstr[1] "Protsessorid:"

#: CPUEntry.cpp:22
#, kde-format
msgctxt ""
"unknown CPU type/product name; presented as `Processors: 4 × Unknown Type'"
msgid "Unknown Type"
msgstr "tundmatu tüüp"

#: GPUEntry.cpp:19
#, kde-format
msgid "Graphics Processor:"
msgstr "Graafikaprotsessor:"

#: GraphicsPlatformEntry.cpp:10
#, kde-format
msgid "Graphics Platform:"
msgstr "Graafikaplatvorm:"

#: KernelEntry.cpp:11
#, kde-format
msgid "Kernel Version:"
msgstr "Kerneli versioon:"

#: KernelEntry.cpp:23
#, kde-format
msgctxt "@label %1 is the kernel version, %2 CPU bit width (e.g. 32 or 64)"
msgid "%1 (%2-bit)"
msgstr "%1 (%2-bitine)"

#: main.cpp:174
#, kde-format
msgctxt "@label"
msgid "Manufacturer:"
msgstr "Tootja:"

#: main.cpp:177 main.cpp:194
#, kde-format
msgctxt "@label"
msgid "Product Name:"
msgstr "Mudel:"

#: main.cpp:180
#, kde-format
msgctxt "@label"
msgid "System Version:"
msgstr "Süsteemi versioon:"

#: main.cpp:183 main.cpp:197
#, kde-format
msgctxt "@label"
msgid "Serial Number:"
msgstr "Seerianumber:"

#: main.cpp:187 main.cpp:204
#, kde-format
msgctxt "@label unknown entry in table"
msgid "Unknown:"
msgstr "Teadmata"

#: main.cpp:200
#, kde-format
msgctxt "@label uboot is the name of a bootloader for embedded devices"
msgid "U-Boot Version:"
msgstr "U-Booti versioon:"

#: main.cpp:238
#, kde-format
msgid "KDE Frameworks Version:"
msgstr "KDE Frameworksi versioon:"

#: main.cpp:239
#, kde-format
msgid "Qt Version:"
msgstr "Qt versioon:"

#: MemoryEntry.cpp:20
#, kde-format
msgid "Memory:"
msgstr "Mälu:"

#: MemoryEntry.cpp:49
#, kde-format
msgctxt "@label %1 is the formatted amount of system memory (e.g. 7,7 GiB)"
msgid "%1 of RAM"
msgstr "%1 RAM"

#: MemoryEntry.cpp:53
#, kde-format
msgctxt "Unknown amount of RAM"
msgid "Unknown"
msgstr "Teadmata"

#: OSVersionEntry.cpp:9
#, kde-format
msgid "Operating System:"
msgstr "Operatsioonisüsteem:"

#: OSVersionEntry.cpp:11
#, kde-format
msgctxt "@label %1 is the distro name, %2 is the version"
msgid "%1 %2"
msgstr "%1 %2"

#: OSVersionEntry.cpp:13
#, kde-format
msgctxt ""
"@label %1 is the distro name, %2 is the version, %3 is the 'build' which "
"should be a number, or 'rolling'"
msgid "%1 %2 Build: %3"
msgstr "%1 %2, järk: %3"

#: package/contents/ui/main.qml:92
#, kde-format
msgctxt "@title"
msgid "Serial Number"
msgstr "Seerianumber"

#: package/contents/ui/main.qml:98 package/contents/ui/main.qml:179
#, kde-format
msgctxt "@action:button"
msgid "Copy to Clipboard"
msgstr "Kopeeri lõikepuhvrisse"

#: package/contents/ui/main.qml:125
#, kde-format
msgctxt "@action:button show a hidden entry in an overlay"
msgid "Show"
msgstr "Näita"

#: package/contents/ui/main.qml:137
#, kde-format
msgctxt "@title:group"
msgid "Software"
msgstr "Tarkvara"

#: package/contents/ui/main.qml:147
#, kde-format
msgctxt "@title:group"
msgid "Hardware"
msgstr "Riistvara"

#: package/contents/ui/main.qml:167
#, kde-format
msgctxt "@action:button launches kinfocenter from systemsettings"
msgid "Launch %1"
msgstr ""

#: package/contents/ui/main.qml:186
#, kde-format
msgctxt "@action:button"
msgid "Copy to Clipboard in English"
msgstr "Kopeeri lõikepuhvrisse ingliskeelsena"

#: PlasmaEntry.cpp:15
#, kde-format
msgid "KDE Plasma Version:"
msgstr "KDE Plasma versioon:"

#: ThirdPartyEntry.cpp:12
#, kde-format
msgctxt "Unused but needs to be : to avoid assertion in Entry constructor"
msgid ":"
msgstr ""

#~ msgctxt "@action:button launches kinfocenter from systemsettings"
#~ msgid "Show More Information"
#~ msgstr "Näita lisateavet"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Marek Laane"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "qiilaq69@gmail.com"

#~ msgctxt "@title"
#~ msgid "About this System"
#~ msgstr "Süsteemi teave"

#~ msgctxt "@info:credit"
#~ msgid "Copyright 2012-2020 Harald Sitter"
#~ msgstr "Autoriõigus 2012-2020: Harald Sitter"

#~ msgctxt "@info:credit"
#~ msgid "Harald Sitter"
#~ msgstr "Harald Sitter"

#~ msgctxt "@info:credit"
#~ msgid "Author"
#~ msgstr "Autor"

#~ msgid "Copy software and hardware information to clipboard"
#~ msgstr "Kopeeri tark- ja riistvarateave lõikepuhvrisse"

#~ msgid "Copy software and hardware information to clipboard in English"
#~ msgstr "Kopeeri tark- ja riistvarateave lõikepuhvrisse ingliskeelsena"

#~ msgid "OS Type:"
#~ msgstr "OS-i tüüp:"

#~ msgctxt "@title"
#~ msgid "About Distribution"
#~ msgstr "Distributsiooni teave"

#~ msgid "{variantLabel}"
#~ msgstr "{variantLabel}"
