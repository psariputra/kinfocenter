# Translation of kcm_pci into Japanese.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2009.
# Taiki Komoda <kom@kde.gr.jp>, 2010.
# Fumiaki Okushi <okushi@kde.gr.jp>, 2010.
# Jumpei Ogawa <phanective@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcm_pci\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-24 00:45+0000\n"
"PO-Revision-Date: 2010-09-25 17:19-0700\n"
"Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 1.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Yukiko Bando,Taiki Komoda"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ybando@k6.dion.ne.jp,kom@kde.gr.jp"

#: main.cpp:26
#, fuzzy, kde-format
#| msgid "PCI-X"
msgctxt "@label kcm name"
msgid "PCI"
msgstr "PCI-X"

#: main.cpp:27
#, kde-format
msgid "Harald Sitter"
msgstr ""

#: package/contents/ui/main.qml:14
#, fuzzy
#| msgid "Information"
msgctxt "@info:whatsthis"
msgid "PCI Information"
msgstr "情報"
